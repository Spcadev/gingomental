package main

import (
	"github.com/gin-gonic/contrib/secure"
	"github.com/gin-gonic/gin"
)

func main() {
	// secure middleware init
	secureMiddleware := secure.Secure(secure.Options{
		AllowedHosts:          []string{"example.com", "ssl.example.com"},
		SSLRedirect:           true,
		SSLHost:               "ssl.example.com",
		SSLProxyHeaders:       map[string]string{"X-Forwarded-Proto": "https"},
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: "default-src 'self'; script-src 'self' 'unsafe-eval'; connect-src http://localhost:*/* ws://localhost:*/*; img-src 'self'; style-src 'self';",
		IsDevelopment:         true, // Todo: Remove this line in production!
	})

	// initialize bare router
	router := gin.New()

	// explicitly set default middleware
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	// set security middleware
	router.Use(secureMiddleware)

	// set static and templates
	router.Static("/static", "./")

	router.Run(":3000")
}
