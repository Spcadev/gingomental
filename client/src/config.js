System.config({
  baseURL: "/src/",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },
  bundles: {
    "build/main-bundle.js": [
      "application/main.js",
      "npm:page@1.7.1.js",
      "application/controllers/landing.js",
      "npm:rest@1.3.2.js",
      "npm:when@3.7.7.js",
      "npm:most@0.18.8.js",
      "npm:page@1.7.1/index.js",
      "npm:rest@1.3.2/interceptor/mime.js",
      "npm:rest@1.3.2/interceptor/errorCode.js",
      "application/utils/incremental-dom.js",
      "application/utils/jsonml2idom.js",
      "npm:rest@1.3.2/browser.js",
      "npm:when@3.7.7/when.js",
      "npm:path-to-regexp@1.2.1.js",
      "github:jspm/nodelibs-process@0.1.2.js",
      "npm:most@0.18.8/most.js",
      "npm:rest@1.3.2/interceptor.js",
      "npm:rest@1.3.2/mime.js",
      "npm:rest@1.3.2/mime/registry.js",
      "npm:when@3.7.7/lib/decorators/fold.js",
      "npm:when@3.7.7/lib/decorators/flow.js",
      "npm:when@3.7.7/lib/decorators/iterate.js",
      "npm:when@3.7.7/lib/decorators/progress.js",
      "npm:when@3.7.7/lib/decorators/with.js",
      "npm:rest@1.3.2/client/default.js",
      "npm:when@3.7.7/lib/decorators/timed.js",
      "npm:when@3.7.7/lib/decorators/array.js",
      "npm:when@3.7.7/lib/decorators/inspect.js",
      "npm:when@3.7.7/lib/decorators/unhandledRejection.js",
      "npm:rest@1.3.2/client/xhr.js",
      "npm:when@3.7.7/lib/TimeoutError.js",
      "npm:when@3.7.7/lib/apply.js",
      "npm:most@0.18.8/lib/Stream.js",
      "npm:most@0.18.8/lib/base.js",
      "npm:when@3.7.7/lib/Promise.js",
      "npm:most@0.18.8/lib/source/core.js",
      "npm:most@0.18.8/lib/source/from.js",
      "npm:most@0.18.8/lib/combinator/observe.js",
      "npm:most@0.18.8/lib/combinator/loop.js",
      "npm:most@0.18.8/lib/combinator/accumulate.js",
      "npm:most@0.18.8/lib/source/unfold.js",
      "npm:most@0.18.8/lib/source/iterate.js",
      "npm:most@0.18.8/lib/combinator/build.js",
      "npm:most@0.18.8/lib/source/generate.js",
      "npm:most@0.18.8/lib/combinator/transform.js",
      "npm:most@0.18.8/lib/combinator/applicative.js",
      "npm:most@0.18.8/lib/combinator/transduce.js",
      "npm:most@0.18.8/lib/combinator/flatMap.js",
      "npm:most@0.18.8/lib/combinator/continueWith.js",
      "npm:most@0.18.8/lib/combinator/concatMap.js",
      "npm:most@0.18.8/lib/combinator/mergeConcurrently.js",
      "npm:most@0.18.8/lib/combinator/merge.js",
      "npm:most@0.18.8/lib/combinator/combine.js",
      "npm:most@0.18.8/lib/combinator/sample.js",
      "npm:most@0.18.8/lib/combinator/zip.js",
      "npm:most@0.18.8/lib/combinator/filter.js",
      "npm:most@0.18.8/lib/combinator/slice.js",
      "npm:most@0.18.8/lib/combinator/timeslice.js",
      "npm:most@0.18.8/lib/combinator/delay.js",
      "npm:most@0.18.8/lib/combinator/timestamp.js",
      "npm:most@0.18.8/lib/combinator/limit.js",
      "npm:most@0.18.8/lib/combinator/promises.js",
      "npm:@most/multicast@1.0.4.js",
      "npm:most@0.18.8/lib/combinator/errors.js",
      "npm:path-to-regexp@1.2.1/index.js",
      "github:jspm/nodelibs-process@0.1.2/index.js",
      "npm:most@0.18.8/lib/source/periodic.js",
      "npm:most@0.18.8/lib/source/create.js",
      "npm:most@0.18.8/lib/source/fromEvent.js",
      "npm:most@0.18.8/lib/combinator/switch.js",
      "npm:rest@1.3.2/mime/type/application/json.js",
      "npm:rest@1.3.2/mime/type/application/x-www-form-urlencoded.js",
      "npm:rest@1.3.2/mime/type/multipart/form-data.js",
      "npm:rest@1.3.2/mime/type/text/plain.js",
      "npm:rest@1.3.2/mime/type/application/hal.js",
      "npm:rest@1.3.2/client.js",
      "npm:rest@1.3.2/util/mixin.js",
      "npm:rest@1.3.2/util/responsePromise.js",
      "npm:when@3.7.7/lib/format.js",
      "npm:when@3.7.7/lib/state.js",
      "npm:rest@1.3.2/util/normalizeHeaderName.js",
      "npm:rest@1.3.2/UrlBuilder.js",
      "npm:when@3.7.7/lib/env.js",
      "npm:when@3.7.7/lib/Scheduler.js",
      "npm:most@0.18.8/lib/iterable.js",
      "npm:most@0.18.8/lib/sink/Pipe.js",
      "npm:most@0.18.8/lib/Promise.js",
      "npm:most@0.18.8/lib/LinkedList.js",
      "npm:most@0.18.8/lib/invoke.js",
      "npm:most@0.18.8/lib/Queue.js",
      "npm:most@0.18.8/lib/fatalError.js",
      "npm:most@0.18.8/lib/source/tryEvent.js",
      "npm:isarray@0.0.1.js",
      "npm:most@0.18.8/lib/source/ValueSource.js",
      "npm:most@0.18.8/lib/disposable/dispose.js",
      "npm:most@0.18.8/lib/scheduler/PropagateTask.js",
      "npm:most@0.18.8/lib/source/fromArray.js",
      "npm:most@0.18.8/lib/source/fromIterable.js",
      "npm:most@0.18.8/lib/runSource.js",
      "npm:most@0.18.8/lib/fusion/Map.js",
      "npm:most@0.18.8/lib/sink/IndexSink.js",
      "npm:most@0.18.8/lib/fusion/Filter.js",
      "npm:process@0.11.2.js",
      "npm:most@0.18.8/lib/sink/DeferredSink.js",
      "npm:most@0.18.8/lib/source/EventTargetSource.js",
      "npm:most@0.18.8/lib/source/EventEmitterSource.js",
      "npm:when@3.7.7/lib/makePromise.js",
      "npm:@most/multicast@1.0.4/dist/multicast.js",
      "npm:rest@1.3.2/util/find.js",
      "npm:rest@1.3.2/interceptor/pathPrefix.js",
      "npm:rest@1.3.2/interceptor/template.js",
      "npm:rest@1.3.2/util/lazyPromise.js",
      "npm:isarray@0.0.1/index.js",
      "npm:most@0.18.8/lib/disposable/Disposable.js",
      "npm:most@0.18.8/lib/disposable/SettableDisposable.js",
      "npm:most@0.18.8/lib/sink/Observer.js",
      "npm:process@0.11.2/browser.js",
      "npm:most@0.18.8/lib/fusion/FilterMap.js",
      "npm:most@0.18.8/lib/scheduler/defaultScheduler.js",
      "npm:most@0.18.8/lib/defer.js",
      "npm:@most/prelude@1.1.0.js",
      "npm:rest@1.3.2/util/uriTemplate.js",
      "npm:most@0.18.8/lib/scheduler/timeoutTimer.js",
      "npm:most@0.18.8/lib/scheduler/Scheduler.js",
      "npm:@most/prelude@1.1.0/dist/prelude.js",
      "npm:most@0.18.8/lib/scheduler/nodeTimer.js",
      "npm:rest@1.3.2/util/uriEncoder.js"
    ]
  },

  map: {
    "@most/multicast": "npm:@most/multicast@1.0.4",
    "babel": "npm:babel-core@5.8.35",
    "babel-runtime": "npm:babel-runtime@5.8.35",
    "core-js": "npm:core-js@1.2.6",
    "cujojs/most": "github:cujojs/most@0.18.6",
    "jsonml2idom": "npm:jsonml2idom@0.3.2",
    "most": "npm:most@0.18.8",
    "page": "npm:page@1.7.1",
    "paolocaminiti/jsonml2idom": "github:paolocaminiti/jsonml2idom@master",
    "rest": "npm:rest@1.3.2",
    "vue": "npm:vue@1.0.20",
    "vue-router": "npm:vue-router@0.7.11",
    "when": "npm:when@3.7.7",
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-buffer@0.1.0": {
      "buffer": "npm:buffer@3.6.0"
    },
    "github:jspm/nodelibs-events@0.1.1": {
      "events": "npm:events@1.0.2"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-stream@0.1.0": {
      "stream-browserify": "npm:stream-browserify@1.0.0"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:@most/multicast@1.0.4": {
      "@most/prelude": "npm:@most/prelude@1.1.0",
      "most": "npm:most@0.18.8"
    },
    "npm:@most/prelude@1.1.0": {
      "most": "npm:most@0.18.8"
    },
    "npm:amdefine@1.0.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "module": "github:jspm/nodelibs-module@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.35": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:buffer@3.6.0": {
      "base64-js": "npm:base64-js@0.0.8",
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "ieee754": "npm:ieee754@1.1.6",
      "isarray": "npm:isarray@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.6": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-util-is@1.0.2": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:envify@3.4.0": {
      "jstransform": "npm:jstransform@10.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "through": "npm:through@2.3.8"
    },
    "npm:esprima-fb@13001.1001.0-dev-harmony-fb": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:jstransform@10.1.0": {
      "base62": "npm:base62@0.1.1",
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "esprima-fb": "npm:esprima-fb@13001.1001.0-dev-harmony-fb",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "source-map": "npm:source-map@0.1.31"
    },
    "npm:most@0.18.8": {
      "@most/multicast": "npm:@most/multicast@1.0.4",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:page@1.7.1": {
      "path-to-regexp": "npm:path-to-regexp@1.2.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:path-to-regexp@1.2.1": {
      "isarray": "npm:isarray@0.0.1"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:readable-stream@1.1.13": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "core-util-is": "npm:core-util-is@1.0.2",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "isarray": "npm:isarray@0.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "stream-browserify": "npm:stream-browserify@1.0.0",
      "string_decoder": "npm:string_decoder@0.10.31"
    },
    "npm:rest@1.3.2": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0",
      "when": "npm:when@3.7.7"
    },
    "npm:source-map@0.1.31": {
      "amdefine": "npm:amdefine@1.0.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:stream-browserify@1.0.0": {
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "readable-stream": "npm:readable-stream@1.1.13"
    },
    "npm:string_decoder@0.10.31": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:through@2.3.8": {
      "process": "github:jspm/nodelibs-process@0.1.2",
      "stream": "github:jspm/nodelibs-stream@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:vue@1.0.20": {
      "envify": "npm:envify@3.4.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:when@3.7.7": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
