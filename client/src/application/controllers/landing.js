import rest from 'rest';
import mime from 'rest/interceptor/mime';
import errCode from 'rest/interceptor/errorCode';
import when from 'when';
import most from 'most';

import IncrementalDOM from 'application/utils/incremental-dom';
import parse from 'application/utils/jsonml2idom';

var patch = IncrementalDOM.patch;
var client = rest.wrap(mime).wrap(errCode, { code: 500 });
var getJson = when.lift(function(url) { return client({ path: url}); });

// Landing page.
// This function is a basic showcase of the Javascript client reciving a JSONML payload.
// The payload is parsed and added to the DOM via IncrementalDOM with a promise.
// Once added, it can be acted upon by any other javascript; no jQuery call-back hell.
export function loadLanding() {
  getJson('http://localhost:8000/api/landing')
  .then(function(response) {
    return JSON.parse(response.entity.data);
  })
  .then(function(data) {
    var node = document.getElementById('app');
    patch(node, function() {
      parse(data);
    });
  })
  .done(function() {
    landingPage();
  });
}

function landingPage() {
  var xInput = document.querySelector('input.x');
  var yInput = document.querySelector('input.y');
  var resultNode = document.querySelector('.result');

  // x represents the current value of xInput
  var x = most.fromEvent('input', xInput).map(toNumber);

  // y represents the current value of yInput
  var y = most.fromEvent('input', yInput).map(toNumber);

  // result is the live current value of adding x and y
  var result = most.combine(add, x, y);

  // Observe the result value by rendering it to the resultNode
  result.observe(renderResult);

  function add(x, y) {
    return x + y;
  }

  function toNumber(e) {
    return Number(e.target.value);
  }

  function renderResult(result) {
    resultNode.textContent = result;
  }
}