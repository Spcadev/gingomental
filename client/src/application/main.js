import page from 'page';

import { loadLanding } from 'application/controllers/landing';


page('/', loadLanding);
page('*', loadLanding);
page();