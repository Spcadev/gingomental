package gingomental

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Index performs the business logic to render the home page.
func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "layouts/index.html", gin.H{"data": pages["landing.tmpl"]})
}
