package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/contrib/secure"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"
	"github.com/oxtoacart/bpool"
)

// Bufpool implements leaky pools of byte arrays, buffers and bounded channels.
// Bufpool uses buffers to execute HTML templates against and encode JSON.
// If there are errors parsing HTML, this method prevents half-finished pages reaching the client.
var bufpool *bpool.BufferPool

// Pages is a direct maping of layout files to parsed JSONML.
// pages[fileName] = JSONML
var pages = make(map[string]string)
var elements []interface{}

func main() {
	InitView()
	// Secure middleware init.
	secureMiddleware := secure.Secure(secure.Options{
		AllowedHosts:          []string{"example.com", "ssl.example.com"},
		SSLRedirect:           true,
		SSLHost:               "ssl.example.com",
		SSLProxyHeaders:       map[string]string{"X-Forwarded-Proto": "https"},
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: "default-src 'self'; script-src 'self' 'unsafe-eval'; connect-src http://localhost:*/* ws://localhost:*/*; img-src 'self'; style-src 'self';",
		IsDevelopment:         true, // Todo: Remove this line in production!
	})

	corsMiddleware := cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	})

	// Initialize bare router.
	router := gin.New()

	// Explicitly set default middleware.
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	// Set security middleware.
	router.Use(secureMiddleware)
	router.Use(corsMiddleware)

	// API
	// Landing page to display simple working template layout with some javascript.
	// The pages global is loaded with parsed templates once, when the server is started.
	// Layout templates are the keys, and values are JSONML for the server.
	router.GET("/api/landing", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": pages["base.tmpl"]})
	})

	router.Run(":8090")
}
