# GINGOMENTAL: GIN-GONIC/GIN + INCREMENTAL-DOM #

This is a pet project I will be working on *intermittently*, when my spare time has spare time.  The goal is to find a way to utilize Google's [Incremental-DOM](https://github.com/google/incremental-dom) in a way that allows developers to write templates in an 'old-school' fashion (on the server) and still create single page, JavaScript driven web applications.  The backend is [Gin-gonic/Gin](https://github.com/gin-gonic/gin) which is a highly performant, low memory Golang http router/server.

## The Features ##

Templates are written on the server in plain HTML, and can utilize Golang's template inheritance system (or any other feature of the Golang template library).  When the server is started, all templates are parsed and compiled into [JSONML](http://www.jsonml.org/).  This creates a `map[string]string` of layout(key) to jsonml(value) that can be served on demand from memory.  Each response is parsed and patched on the client side by [JSONML2IDOM](https://github.com/paolocaminiti/jsonml2idom).

In theory, the benefits would be...

| Status     | Feature |
|------------|------------------------------------------------------------------------------------|
| Completed  | **Easy Templates:** No template language, JSX or anything else required.  Just start writing plain, effortless HTML the way it has always been done. |
| Completed  | **Golang Templates:** All the *optional* power of Go templates (including inheritance) in a single page application. |
| Partial    | **Server-side Rendering:** Templates can still be rendered the old-fashioned way for bots or users with Javascript disabled. |
| Todo       | **Chunking:** Although templates can be compiled completely, this would also allow portions of templates, or *components* to be called on demand, with data already initialized.  |
| Todo       | **Component Support:** Incremental-DOM [can be used with components](https://github.com/google/incremental-dom/blob/master/demo/define_component.js).  A way needs to be found for components to be declared on the server in a self-contained file (HTML/JS/CSS), parsed into composite parts and served on demand.  Similar to Vue.js.  This can probably be achieved via the `golang.org/x/net/html` package, extracting content from `<script>` and `<style>` tags for safe transmission over the wire as JSON. |

## Additional Goals ##

This setup has a mobile first mindset: Incremental-DOM is a very lightweight (5kb minified) library that has a low memory footprint, while still being [performant](https://auth0.com/blog/2015/11/20/face-off-virtual-dom-vs-incremental-dom-vs-glimmer/).  The Javascript portion of the site should follow suit and keep a low profile as well.  I'll be using [Page](https://visionmedia.github.io/page.js/) for routing, with [Rest](https://github.com/cujojs/rest) and [When](https://github.com/cujojs/when) for promise based calls to the server.  I haven't figured out the data layer for the client yet, so that's definitely a work in progress.

CSS Sidenote: Bootstrap and jQuery, although the stuff of legends, are quite bulky for simple sites.  I've recently come across [Bulma](http://bulma.io/) which is a clean, lightweight CSS grid framework in early development that looks quite promising.  Definitely going to give this a shot in future projects.  The reason for this thinking is that beyond the bulk of Bootstrap, they also favour a ton of elements.  Incremental-DOM *walks each element when doing a diff*, so the fewer elements the better.

## Installation ##

This project really isn't ready for even experimental use yet, but if you're curious or want to play with it...

This uses my [GOJS Starter Kit](https://bitbucket.org/Spcadev/gojs-starter-kit/src/95d8dd188509eba07beadb40ba51789cb01f6ead/readme.md?fileviewer=file-view-default) with a slightly different directory structure that emphasizes Golang templates.  Base templates go into the `templates/layouts` directory and composite templates (extended) go into the `templates/components` directory, which will eventually hold web components once support for that is in.

[License: MIT](https://bitbucket.org/Spcadev/gingomental/src/84852ff7f125d8d5be1835518128f20a4df4e48a/license.md?fileviewer=file-view-default)